package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LIST = "commodity.lst";
	
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_BRANCH_NAME_ERROR = "の支店コードが不正です";
	private static final String SALES_FILE_PRODUCT_NUMBER_ERROR = "の商品コードが不正です";
	private static final String FILE_FORMAT_ERROR = "のフォーマットが不正です";
	private static final String FILE_NOT_SUCCESSIVE ="売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_OVER = "合計金額が10桁を超えました";
	
	//正規表現
	private static final String BRANCH_CODE = "^[0-9]{3}$";
	private static final String PRODUCT_CODE = "^[0-9a-zA-Z]{8}$";
	private static final String FILE_EXTENTION = "^[0-9]{8}.rcd$";
	private static final String NUMBER_OR_NOT = "^[0-9]+$";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//エラー処理3-1　コマンドライン引数がない場合
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		
		//商品コードと商品名を保持するMap
		Map<String,String> commodityItems = new HashMap<>();
		//商品コードと売上額を保持するMap
		Map<String,Long> commoditySales = new HashMap<>();

		String branch = "支店定義ファイル";
		String commodity = "商品定義ファイル";
		
		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, BRANCH_CODE, branch, branchNames, branchSales)) {
			return;
		}
		
		//商品定義ファイル読み込み処理
		//1-3商品定義ファイルを開く
		if (!readFile(args[0], FILE_NAME_COMMODITY_LIST, PRODUCT_CODE, commodity, commodityItems, commoditySales)) {
			return;
		}
		
		
		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		String fileExtention = FILE_EXTENTION;
		List<File> rcdFiles = new ArrayList<>();

		//エラー処理3-3　ファイルなのかどうか確認する
		for (int i = 0; i < files.length; i++) {

			if (files[i].isFile() && files[i].getName().matches(fileExtention)) {
				rcdFiles.add(files[i]);
			}
			
		}

		//エラー処理2-1 rcdファイルは連番か
		Collections.sort(rcdFiles);

		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int later = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((later - former) != 1) {
				System.out.println(FILE_NOT_SUCCESSIVE);
				return;
			}

		}

		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {

			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				List<String> sales = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					sales.add(line);
				}
				
				//エラー処理2-4　売上ファイルの中身が２行でなかった場合
				//商品定義2-5　rcdファイルの中身が３行じゃなかったらに変更
				if (sales.size() != 3) {
					System.out.println(rcdFiles.get(i) + FILE_FORMAT_ERROR);
					return;
				}
				
				
				//エラー処理2-3　rcdファイル支店コードが支店定義ファイルに該当しなかった場合
				if (!branchNames.containsKey(sales.get(0))) {
					System.out.print(rcdFiles.get(i) + FILE_BRANCH_NAME_ERROR);
					return;
				}
				
				//エラー処理2-4　売上ファイルの商品コードが商品定義ファイルに該当しなかった場合
				if(!commodityItems.containsKey(sales.get(1))) {
					System.out.println(rcdFiles.get(i) + SALES_FILE_PRODUCT_NUMBER_ERROR);
					return;
				}
				
				//エラー処理3-2　売上金額が数字かどうか
				if (!sales.get(2).matches( NUMBER_OR_NOT)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long salesAmount = Long.parseLong(sales.get(2)) + branchSales.get(sales.get(0));
				Long commoditySalesAmount = Long.parseLong(sales.get(2)) + commoditySales.get(sales.get(1));
				
				//エラー処理 2-2 売上金額10桁超えた場合
				if (salesAmount >= 10000000000L || commoditySalesAmount >= 10000000000L) {
					System.out.println(SALES_AMOUNT_OVER);
					return;
				}
				
				branchSales.put(sales.get(0), salesAmount);
				//商品定義2-2　読み込んだ売り上げファイルから商品コード、該当する商品の合計金額にそれぞれ加算
				commoditySales.put(sales.get(1), commoditySalesAmount);
				
			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						br.close();

					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}

				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		
		//商品定義3-2 集計結果出力　
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityItems, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String regularExpression, String name, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		BufferedReader br = null;

		try {

			File file = new File(path, fileName);

			//エラー処理1-1　ファイルは存在するか
			if (!file.exists()) {
				System.out.println(name + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			int i = 0;

			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				
				//1-4商品定義ファイルを一行ずつ読み込み、すべての商品コードとそれに対応する商品名を保持する
				//エラー処理1-2　ファイルのフォーマットは正しいか
				if ((items.length != 2) || (!items[i].matches(regularExpression))) {
					System.out.println(name + FILE_FORMAT_ERROR);
					return false;
				}

				branchNames.put(items[i], items[i + 1]);
				branchSales.put(items[i], 0L);
				

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {

		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File file = new File(path, fileName);
		BufferedWriter bw = null;

		try {

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//キー取得
			for (String key : branchSales.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
